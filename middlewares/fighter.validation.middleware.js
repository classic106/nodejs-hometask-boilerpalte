const { fighter } = require('../models/fighter');

function validation(body, action = '') {

    if(action){
      action = `to ${action}`
    }
    if(!body.health){
      body.health = 100;
    }
  
    for (const [key, value] of Object.entries(body)) {
      if (!fighter.hasOwnProperty(key)) {
        throw Error(`Fighter entity ${action} isn't valid`);
      }
      if (key === "power" && (1 > value || value > 100)) {
        throw Error("Fighter power isn't valid");
      }
      if (key === "defense" && (1 > value || value > 10)) {
        throw Error("Fighter defense isn't valid");
      }
      if (key === "health" && (80 > value || value > 120)) {
        throw Error("Fighter health isn't valid");
      }
    }
  }

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const { body } = req;

    try {
      const hasAllFields = (()=>{
        let b = Object.keys(body);
        let f = Object.keys(fighter);
  
        return f.reduce((acc, val)=>{
          if(!acc || val === 'id' || val === 'health') return acc;
          return b.includes(val);
        }, true);
      })();

      if(!hasAllFields){
        throw Error("Fighter entity to create isn't valid");
      };

      validation(body, 'create');
    } catch (err) {
      res.err = err;
    } finally {
      next();
    }
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const { body } = req;

    try {
      validation(body, 'update');
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;