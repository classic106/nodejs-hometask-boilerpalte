const { user } = require("../models/user");

function validation(body, action='') {
  if(action){
    action = `to ${action}`
  }

  for (const [key, value] of Object.entries(body)) {
    if (!user.hasOwnProperty(key)) {
      throw Error(`User entity ${action} isn't valid`);
    }
    if (key === "password" && value.length < 3) {
      throw Error("User password isn't valid");
    }
    if (key === "email" && !value.includes("@gmail")) {
      throw Error("User email isn't valid");
    }
    if (key === "phoneNumber" && !new RegExp(/^\+380\d{9}$/).test(value)) {
      throw Error("User phoneNumber isn't valid");
    }
  }
}

const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  const { body } = req;

  try {
    const hasAllFields = (()=>{
      let b = Object.keys(body);
      let u = Object.keys(user);

      return u.reduce((acc, val)=>{
        if(!acc || val === 'id') return acc;
        return b.includes(val);
      }, true);
    })();
    
    if(!hasAllFields){
      throw Error("User entity to create isn't valid");
    };

    validation(body, 'create');
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  const { body } = req;

  try {
    if(Object.keys(body).length < 1){
      throw Error("User entity to create isn't valid");
    }
    validation(body, 'update');
  } catch (err) {
    res.err = err;
  } finally {
    next();
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
