const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
   const { data, err } = res;

   if(err){
      const { message } = err;
      if(message.includes('not found')){
         res.status(404).json({error: true, message: err.message});
         return;
      }
      
      res.status(400).json({error: true, message: err.message});
      return;
   }else if(data){
      res.status(200).json(data);
      return;
   }

   next();
}

exports.responseMiddleware = responseMiddleware;