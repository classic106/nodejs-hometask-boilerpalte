const { UserRepository } = require("../repositories/userRepository");

class UserService {
  // TODO: Implement methods to work with user
  search(search) {

    if(!Object.keys(search).length || !search){
      throw Error('User not found');
    }

    const item = UserRepository.getOne(search);
    if (!item) {
      throw Error('User not found');
    }
    return item;
  }

  getAll() {
    const items = UserRepository.getAll();
    return items;
  }

  create(body) {
    if (!Object.keys(body).length || !body || body.id) {
      throw Error("User entity to create isn't valid");
    }

    const { email, phoneNumber } = body;

    const isMailExist = UserRepository.getOne({ email });
    const isPhoneNumberExist = UserRepository.getOne({ phoneNumber });

    if (isMailExist || isPhoneNumberExist) {
      throw Error(`User with email: ${email} or phoneNumber: ${phoneNumber} is already exist`);
    }

    return UserRepository.create(body);
  }

  update(id, body) {
    if (!Object.keys(body).length || !body || body.id || !id) {
      throw Error("User entity to create isn't valid");
    }

    const user = UserRepository.getOne({ id });

    if (!user) {
      throw Error("User not found");
    }

    return UserRepository.update(id, body);
  }

  delete(id) {
    if(!id){
      throw Error('User not found');
    }

    const item = UserRepository.delete(id);
    
    if (!item) {
      throw Error('User not found');
    }
    return item;
  }
}

module.exports = new UserService();
