const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    search(search) {
        if(!Object.keys(search).length){
          throw Error('Fighter not found');
        }

        const item = FighterRepository.getOne(search);
        
        if (!item) {
          return null;
        }
        return item;
      }
    
      getAll() {
        const items = FighterRepository.getAll();
        return items;
      }
    
      create(body) {
        if (!Object.keys(body).length || body.id) {
          throw Error("Fighter entity to create isn't valid");
        }
        
        const { name } = body;

        const fighter = FighterRepository.getOne({ name });
  
        if (fighter) {
          throw Error(`Fighter with ${name} is already exist`);
        }

        return FighterRepository.create(body);
      }
    
      update(id, body) {
        if (!Object.keys(body).length || body.id || !body || !id) {
          throw Error("Fighter entity to update isn't valid");
        }

        const fighter = FighterRepository.getOne({ id });

        if (!fighter) {
          throw Error("Fighter not found");
        }

        return FighterRepository.update(id, body);
      }
    
      delete(id) {
        if (!id) {
          throw Error("Fighter not found");
        }

        const item = FighterRepository.delete(id);

        if (!item) {
          throw Error("Fighter not found");
        }
        return item;
      }
}

module.exports = new FighterService();