const UserService = require('./userService');

class AuthService {
    login(userData) {
        if(!Object.keys(userData).length){
            throw Error('User entity to auth isn\'t valid');
        }

        const user = UserService.search(userData);
        if(!user) {
            throw Error('User not found');
        }
        return user;
    }
}

module.exports = new AuthService();